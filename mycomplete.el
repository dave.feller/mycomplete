;;; mycomplete.el --- my icomplete hacks -*- lexical-binding: t -*-

(require 'rfn-eshadow) ; rfn-eshadow-overlay
(require 'simple) ; max-mini-window-lines
(require 'cl-lib)

(defgroup mycomplete nil
  "Show completions dynamically in minibuffer."
  :prefix "mycomplete-"
  :group 'mycomplete)

(defcustom mycomplete-separator " | "
  "String used by Icomplete to separate alternatives in the minibuffer."
  :type 'string
  :version "24.4")

(defcustom mycomplete-show-matches-on-no-input nil
  "When non-nil, show completions when first prompting for input.
This means to show completions even when the current minibuffer contents
is the same as was the initial input after minibuffer activation.
This also means that if you traverse the list of completions with
commands like \\`C-.' and just hit \\`RET' without typing any
characters, the match under point will be chosen instead of the
default."
  :type 'boolean
  :version "24.4")

(defcustom mycomplete-with-completion-tables t
  "Specialized completion tables with which Icomplete should operate.
If this is t, Icomplete operates on all tables.
Otherwise this should be a list of the completion tables (e.g.,
`internal-complete-buffer') on which Icomplete should operate."
  ;; Prior to 24.4, not a user-option, default '(internal-complete-buffer).
  :version "24.4"
  :type '(choice (const :tag "All" t)
		 (repeat function))
  :group 'mycomplete)

(defcustom mycomplete-matches-format "%s/%s"
  "Format of the current/total number of matches for the prompt prefix."
  :version "28.1"
  :type '(choice (const :tag "No prefix" nil)
                 (string :tag "Prefix format string")))

(defface mycomplete-first-match '((t :weight bold))
  "Face used by Icomplete for highlighting first match."
  :version "24.4")

;;;_* User Customization variables
(defcustom mycomplete-prospects-height 1
  ;; We used to compute how many lines 100 characters would take in
  ;; the current window width, but the return value of `window-width'
  ;; is unreliable on startup (e.g., if we're in daemon mode), so now
  ;; we simply base the default value on an 80 column window.
  "Maximum number of lines to use in the minibuffer."
  :type 'integer
  :version "26.1")

(defcustom mycomplete-minibuffer-setup-hook nil
  "Icomplete-specific customization of minibuffer setup.

This hook is run during minibuffer setup if Icomplete is active.
It is intended for use in customizing Icomplete for interoperation
with other features and packages.  For instance:

  (add-hook \\='icomplete-minibuffer-setup-hook
	     (lambda () (setq-local max-mini-window-height 3)))

will constrain Emacs to a maximum minibuffer height of 3 lines when
icompletion is occurring."
  :type 'hook)

(defvar mycomplete-overlay (make-overlay (point-min) (point-min) nil t t)
  "Overlay used to display the list of completions.")

(defvar mycomplete--initial-input nil
  "Initial input in the minibuffer when `icomplete-mode' was activated.
Used to implement the option `icomplete-show-matches-on-no-input'.")

(defvar mycomplete--index 0
  "Index of currently selected completion.")

(defvar-keymap mycomplete-minibuffer-map
  "C-n" 'mycomplete-forward-completions
  "C-p" 'mycomplete-backward-completions
  "C-s" 'mycomplete-forward-completions
  "C-r" 'mycomplete-backward-completions
  "RET" 'mycomplete-fido-ret
  "C-d" 'mycomplete-fido-delete-char
  "DEL" 'mycomplete-fido-backward-updir
  "C-k" 'mycomplete-fido-kill
  "C-M-j" 'mycomplete-ret
  "C-w" 'mycomplete-copy-or-kill)

(defun mycomplete-post-command-hook ()
  (let ((non-essential t))            ;E.g. don't prompt for password!
    (mycomplete-exhibit)))

(defun mycomplete--sorted-completions ()
    (or completion-all-sorted-completions
        (cl-loop
         initially (setq mycomplete--index 0) ; Invalidate index state
         with beg = (mycomplete--field-beg)
         with end = (mycomplete--field-end)
         with all = (completion-all-sorted-completions beg end)
         ;; Icomplete mode re-sorts candidates, bubbling the default to
         ;; top if it's found somewhere down the list.  This loop's
         ;; iteration variable, `fn' iterates through these "bubble up
         ;; predicates" which may vary depending on specific
         ;; `completing-read' invocations, described below:
         for fn in (cond ((and minibuffer-default
                               (stringp (or (car-safe minibuffer-default)
                                            minibuffer-default)) ; bug#38992 bug#55800
                               (equal (mycomplete--field-string) mycomplete--initial-input))
                          ;; Here, we have a non-nil string default and
                          ;; no input whatsoever.  We want to make sure
                          ;; that the default is bubbled to the top so
                          ;; that `icomplete-force-complete-and-exit'
                          ;; will select it.  We want to do that even if
                          ;; the match doesn't match the completion
                          ;; perfectly.
                          ;;
                          `(;; The first predicate ensures that:
                            ;;
                            ;; (completing-read "thing? " '("foo" "bar")
                            ;;                  nil nil nil nil "bar")
                            ;;
                            ;; Has "bar" at the top, so RET will select
                            ;; it, as desired.
                            ,(lambda (comp)
                               (equal (or (car-safe minibuffer-default)
                                          minibuffer-default)
                                      comp))
                            ;; Why do we need this second predicate?
                            ;; Because that'll make things like M-x man
                            ;; RET RET, when invoked with point on the
                            ;; "bar" word, behave correctly.  There, the
                            ;; default doesn't quite match any
                            ;; candidate. So:
                            ;;
                            ;; (completing-read "Man entry? " '("foo(1)" "bar(1)")
                            ;;                  nil nil nil nil "bar")
                            ;;
                            ;; Will place "bar(1)" on top, and RET will
                            ;; select it -- again, as desired.
                            ;;
                            ;; FIXME: it's arguable that this second
                            ;; behavior should be a property of the
                            ;; completion table and not the completion
                            ;; frontend such as we have done
                            ;; here. However, it seems generically
                            ;; useful for a very broad spectrum of
                            ;; cases.
                            ,(lambda (comp)
                               (string-prefix-p (or (car-safe minibuffer-default)
                                                    minibuffer-default)
                                                comp))))
                         ((and (not minibuffer-default)
                               (eq (mycomplete--category) 'file))
                          ;; When there isn't a default, `fido-mode'
                          ;; specifically also has some extra
                          ;; file-sorting semantics inherited from Ido.
                          ;; Those make the directory "./" bubble to the
                          ;; top (if it exists).  This makes M-x dired
                          ;; RET RET go to the directory of current
                          ;; file, which is non-Icomplete vanilla Emacs
                          ;; and `ido-mode' both do.
                          `(,(lambda (comp)
                               (string= "./" comp)))))
         ;; After we have setup the predicates, look for a completion
         ;; matching one of them and bubble up it, destructively on
         ;; `completion-all-sorted-completions' (unless that completion
         ;; happens to be already on top).
         thereis (or
                  (and (funcall fn (car all)) all)
                  (cl-loop
                   for l on all
                   while (consp (cdr l))
                   for comp = (cadr l)
                   when (funcall fn comp)
                   do (setf (cdr l) (cddr l))
                   and return
                   (completion--cache-all-sorted-completions beg end (cons comp all))))
         finally return all)))

(defun mycomplete-completions (candidates predicate require-match)
  "Identify prospective candidates for minibuffer completion.

The display is updated with each minibuffer keystroke during
minibuffer completion.

Prospective completion suffixes (if any) are displayed, bracketed by
one of (), [], or {} pairs.  The choice of brackets is as follows:

  (...) - a single prospect is identified and matching is enforced,
  [...] - a single prospect is identified but matching is optional, or
  {...} - multiple prospects, separated by commas, are indicated, and
          further input is required to distinguish a single one.

If there are multiple possibilities, `icomplete-separator' separates them.

The displays for unambiguous matches have ` [Matched]' appended
\(whether complete or not), or ` [No matches]', if no eligible
matches exist."
  (let* ((ignored-extension-re
          (and minibuffer-completing-file-name
               mycomplete-with-completion-tables
               completion-ignored-extensions
               (concat "\\(?:\\`\\.\\./\\|"
                       (regexp-opt completion-ignored-extensions)
                       "\\)\\'")))
         (minibuffer-completion-table candidates)
         (minibuffer-completion-predicate
          (if ignored-extension-re
              (lambda (cand)
                (and (not (string-match ignored-extension-re cand))
                     (or (null predicate)
                         (funcall predicate cand))))
            predicate))
         ;; (md (completion--field-metadata (mycomplete--field-beg)))
         (comps (mycomplete--sorted-completions))
         (open-bracket (if require-match "(" "["))
         (close-bracket (if require-match ")" "]"))
         (before-string (and mycomplete-matches-format
                             (let ((total (safe-length completion-all-sorted-completions)))
                               (if (= total 1)
                                   (format "%sMatch%s" open-bracket close-bracket)
                                 (format (concat "%s" mycomplete-matches-format "%s")
                                         open-bracket
                                         (1+ mycomplete--index)
                                         (if (> total 9999) "Many" total)
                                         close-bracket))))))
    ;; `concat'/`mapconcat' is the slow part.
    (if (not (consp comps))
        (if before-string
            (cons (format "%sNone%s   " open-bracket close-bracket) " ")
          (cons nil (format " %sNo Matches%s" open-bracket close-bracket)))
      (let* ((last (if (consp comps) (last comps)))
             ;; Save the "base size" encoded in `comps' then
             ;; removing making `comps' a proper list.
             (base-size (prog1 (cdr last)
                          (if last (setcdr last nil))))
             (ellipsis (if (char-displayable-p ?…) "…" "..."))
             (prospects-len (+ (string-width mycomplete-separator)
                               (+ 3 (string-width ellipsis)) ;; take {…} into account
                               (string-width (buffer-string))
                               (string-width (or before-string ""))))
             (prospects-max
              ;; Max total length to use, including the minibuffer content.
              (* (+ mycomplete-prospects-height
                    ;; If the minibuffer content already uses up more than
                    ;; one line, increase the allowable space accordingly.
                    (/ prospects-len (window-width)))
                 (window-width)))
             prospects comp limit)
        (prog1
            (progn
              (while (and comps (not limit))
                (setq comp (car comps)
                      comps (cdr comps))
                (setq prospects-len
                      (+ (string-width comp)
                         (string-width mycomplete-separator)
                         prospects-len))
                (if (< prospects-len prospects-max)
                    (push comp prospects)
                  (setq limit t)
                  ;; Always show the current candidate
                  (when (= 0 (length prospects))
                    (push comp prospects))))
              (setq prospects (nreverse prospects))
              ;; Decorate first of the prospects.
              (when prospects
                (let ((first (copy-sequence (pop prospects))))
                  (put-text-property 0 (length first)
                                     'face 'mycomplete-first-match first)
                  (push first prospects)))
              (cons
               (when before-string (format "%-8s " before-string))
               (concat " {"
                       (mapconcat #'identity prospects mycomplete-separator)
                       (concat (and limit (concat mycomplete-separator ellipsis))
                               "}"))))
          ;; Restore the base-size info, since completion-all-sorted-completions
          ;; is cached.
          (if last (setcdr last base-size)))))))

(defun mycomplete-exhibit ()
  "Insert Icomplete completions display.
Should be run via minibuffer `post-command-hook'.
See `icomplete-mode' and `minibuffer-setup-hook'."
  (when (and mycomplete-mode
             (mycomplete-simple-completing-p)) ;Shouldn't be necessary.
    (save-excursion
      (goto-char (mycomplete--field-end))
                                        ; Insert the match-status information:
      (when (or mycomplete-show-matches-on-no-input
                (not (equal (mycomplete--field-string)
                            mycomplete--initial-input)))
        (let* ((field-string (mycomplete--field-string))
               (text (while-no-input
                       (mycomplete-completions
                        (mycomplete--completion-table)
                        (mycomplete--completion-predicate)
                        (when (window-minibuffer-p)
                          (eq minibuffer--require-match t)))))
               (buffer-undo-list t)
               deactivate-mark)
          ;; Do nothing if while-no-input was aborted.
          (unless (memq text '(t nil)) 
            (move-overlay mycomplete-overlay (point-min) (point) (current-buffer))
            ;; The current C cursor code doesn't know to use the overlay's
            ;; marker's stickiness to figure out whether to place the cursor
            ;; before or after the string, so let's spoon-feed it the pos.
            (put-text-property 0 1 'cursor t (cdr text))
            (overlay-put mycomplete-overlay 'before-string (car text))
            (overlay-put mycomplete-overlay 'after-string (cdr text))))))))

(defun mycomplete-simple-completing-p ()
  "Non-nil if current window is a minibuffer that's doing simple completion.

Conditions are:
   the selected window is a minibuffer,
   and not in the middle of macro execution,
   and the completion table is not a function (which would
       indicate some non-standard, non-simple completion mechanism,
       like file-name and other custom-func completions),
   and `icomplete-with-completion-tables' doesn't restrict completion."
  (unless executing-kbd-macro
    (let ((table (mycomplete--completion-table)))
      (and table
           (or (not (functionp table))
               (eq mycomplete-with-completion-tables t)
               (member table mycomplete-with-completion-tables))))))

(defun mycomplete-minibuffer-setup ()
  "Run in minibuffer on activation to establish incremental completion.
Usually run by inclusion in `minibuffer-setup-hook'."
  (when (and mycomplete-mode (mycomplete-simple-completing-p))
    (setq-local mycomplete--initial-input (mycomplete--field-string))
    (setq-local completion-show-inline-help nil)
    (use-local-map (make-composed-keymap mycomplete-minibuffer-map
    					 (current-local-map)))
    (add-hook 'post-command-hook #'mycomplete-post-command-hook nil t)
    (run-hooks 'mycomplete-minibuffer-setup-hook)))

(defun mycomplete--completion-table ()
  (if (window-minibuffer-p) minibuffer-completion-table
    (or (nth 2 completion-in-region--data)
	(message "In %S (w=%S): %S"
		 (current-buffer) (selected-window) (window-minibuffer-p)))))

(defun mycomplete--completion-predicate ()
  (if (window-minibuffer-p) minibuffer-completion-predicate
    (nth 3 completion-in-region--data)))

(defun mycomplete--field-string ()
  (if (window-minibuffer-p)
      (minibuffer-contents)
    (buffer-substring-no-properties
     (nth 0 completion-in-region--data)
     (nth 1 completion-in-region--data))))

(defun mycomplete--field-beg ()
  (if (window-minibuffer-p) (minibuffer-prompt-end)
    (nth 0 completion-in-region--data)))

(defun mycomplete--field-end ()
  (if (window-minibuffer-p) (point-max)
    (nth 1 completion-in-region--data)))

(defun mycomplete--category ()
  (let* ((beg (mycomplete--field-beg))
         (md (completion--field-metadata beg)))
    (alist-get 'category (cdr md))))

;;;_ > icomplete-mode (&optional prefix)
;;;###autoload
(define-minor-mode mycomplete-mode
  "Toggle incremental minibuffer completion (Icomplete mode).

When this global minor mode is enabled, typing in the minibuffer
continuously displays a list of possible completions that match
the string you have typed.  See `icomplete-completions' for a
description of how prospective completions are displayed.

For more information, see Info node `(emacs)Icomplete'.
For options you can set, `\\[customize-group] icomplete'.

You can use the following key bindings to navigate and select
completions:

\\{icomplete-minibuffer-map}"
  :global t
  (remove-hook 'minibuffer-setup-hook #'mycomplete-minibuffer-setup)
  (when mycomplete-mode
    (add-hook 'minibuffer-setup-hook #'mycomplete-minibuffer-setup)))

(defun mycomplete-forward-completions ()
  "Step forward completions by one entry.
Second entry becomes the first and can be selected with
`icomplete-force-complete-and-exit'.
Return non-nil if something was stepped."
  (interactive)
  (let* ((beg (mycomplete--field-beg))
         (end (mycomplete--field-end))
         (comps (completion-all-sorted-completions beg end)))
    (when (consp (cdr comps))
      (let* ((last (last comps))
             (base (cdr last)))
        (setcdr last nil)
        (setq mycomplete--index (mod (1+ mycomplete--index)
                                     (length comps)))
        (setcdr (last comps) (cons (pop comps) base)))
      (completion--cache-all-sorted-completions beg end comps))))

(defun mycomplete-backward-completions ()
  "Step backward completions by one entry.
Last entry becomes the first and can be selected with
`icomplete-force-complete-and-exit'.
Return non-nil if something was stepped."
  (interactive)
  (let* ((beg (mycomplete--field-beg))
         (end (mycomplete--field-end))
         (comps (completion-all-sorted-completions beg end))
         last-but-one)
    (prog1
        (when (consp (cdr (setq last-but-one (last comps 2))))
          ;; At least two elements in comps
          (let ((base (cddr last-but-one))
                (last (cdr last-but-one)))
            (setcdr last nil)
            (setq mycomplete--index (mod (1- mycomplete--index)
                                         (length comps)))
            (setcdr last base)
            (push (car (cdr last-but-one)) comps)
            (setcdr last-but-one (cdr (cdr last-but-one)))))
      (completion--cache-all-sorted-completions beg end comps))))

(defun mycomplete-force-complete-and-exit ()
  "Complete the minibuffer with the longest possible match and exit.
Use the first of the matches if there are any displayed, and use
the default otherwise."
  (interactive)
  ;; This function is tricky.  The mandate is to "force", meaning we
  ;; should take the first possible valid completion for the input.
  ;; However, if there is no input and we can prove that that
  ;; coincides with the default, it is much faster to just call
  ;; `minibuffer-complete-and-exit'.  Otherwise, we have to call
  ;; `minibuffer-force-complete-and-exit', which needs the full
  ;; completion set and is potentially slow and blocking.  Do the
  ;; latter if:
  (if (or
       ;; there's some input, meaning the default in off the table by
       ;; definition; OR
       (not (equal (mycomplete--field-string) mycomplete--initial-input))
       ;; there's no input, but there's also no minibuffer default
       ;; (and the user really wants to see completions on no input,
       ;; meaning he expects a "force" to be at least attempted); OR
       (and (not minibuffer-default)
            mycomplete-show-matches-on-no-input)
       ;; there's no input but the full completion set has been
       ;; calculated, This causes the first cached completion to
       ;; be taken (i.e. the one that the user sees highlighted)
       completion-all-sorted-completions)
      (if (window-minibuffer-p)
          (minibuffer-force-complete-and-exit)
        (minibuffer-force-complete (mycomplete--field-beg)
                                   (mycomplete--field-end)
                                   'dont-cycle)
        (completion-in-region-mode -1))
    ;; Otherwise take the faster route...
    (if (window-minibuffer-p)
        (minibuffer-complete-and-exit)
      (completion-complete-and-exit
       (mycomplete--field-beg)
       (mycomplete--field-end)
       (lambda () (completion-in-region-mode -1))))))

(defun mycomplete-force-complete ()
  "Complete the icomplete minibuffer."
  (interactive)
  ;; We're not at all interested in cycling here (bug#34077).
  (if (window-minibuffer-p)
      (minibuffer-force-complete nil nil 'dont-cycle)
    (minibuffer-force-complete (mycomplete--field-beg)
                               (mycomplete--field-end)
                               'dont-cycle)))

(defun mycomplete-fido-ret ()
  "Exit minibuffer or enter directory, like `ido-mode'."
  (interactive)
  (let* ((dir (and (eq (mycomplete--category) 'file)
                   (file-name-directory
                    (let ((str (mycomplete--field-string)))
                      (if (and rfn-eshadow-overlay (overlay-buffer rfn-eshadow-overlay))
                          (seq-subseq str (- (overlay-end rfn-eshadow-overlay)
                                             (overlay-start rfn-eshadow-overlay)))
                        str)))))
         (current (car completion-all-sorted-completions))
         (probe (and dir current
                     (expand-file-name (directory-file-name current)
                                       (substitute-env-vars dir)))))
    (cond ((and probe (file-directory-p probe) (not (string= current "./")))
           (mycomplete-force-complete))
          (t
           (mycomplete-force-complete-and-exit)))))

(defun mycomplete-fido-delete-char ()
  "Delete char or maybe call `dired', like `ido-mode'."
  (interactive)
  (let ((end (mycomplete--field-end)))
    (if (or (< (point) end) (not (eq (mycomplete--category) 'file)))
        (call-interactively 'delete-char)
      (dired (file-name-directory (mycomplete--field-string)))
      (exit-minibuffer))))

(defun mycomplete-fido-backward-updir ()
  "Delete char before or go up directory, like `ido-mode'."
  (interactive)
  (cond ((and (eq (char-before) ?/)
              (eq (mycomplete--category) 'file))
         (when (string-equal (mycomplete--field-string) "~/")
           (delete-region (mycomplete--field-beg) (mycomplete--field-end))
           (insert (expand-file-name "~/"))
           (goto-char (line-end-position)))
         (save-excursion
           (goto-char (1- (point)))
           (when (search-backward "/" (point-min) t)
             (delete-region (1+ (point)) (point-max)))))
        (t (call-interactively 'backward-delete-char))))

(defun mycomplete-fido-kill ()
  "Kill line or current completion, like `ido-mode'.
If killing to the end of line make sense, call `kill-line',
otherwise kill the currently selected completion candidate.
Exactly what killing entails is dependent on the things being
completed.  If completing files, it means delete the file.  If
completing buffers it means kill the buffer.  Both actions
require user confirmation."
  (interactive)
  (let ((end (mycomplete--field-end)))
    (if (< (point) end)
        (call-interactively 'kill-line)
      (let* ((all (completion-all-sorted-completions))
             (thing (car all))
             (cat (let ((cat (mycomplete--category)))
                    (if (not (eq cat 'multi-category))
                        cat
                      ;; Strip tofu characters added by consult
                      (let* ((end (length thing)))
                        (while (and (> end 0) (consult--tofu-p (aref thing (1- end))))
                          (cl-decf end))
                        (setq thing (seq-subseq thing 0 end)))
                      (car (get-text-property 0 'multi-category thing)))))
             (action
              (cl-case cat
                (buffer
                 (lambda ()
                   (when (yes-or-no-p (concat "Kill buffer " thing "? "))
                     (kill-buffer thing))))
                ((project-file file)
                 (lambda ()
                   (let* ((dir (file-name-directory (mycomplete--field-string)))
                          (path (expand-file-name thing dir)))
                     (when (yes-or-no-p (concat "Delete file " path "? "))
                       (delete-file path) t))))
                (t
                 (error "Sorry, don't know how to kill things for `%s'" cat)))))
        (when (let (;; Allow `yes-or-no-p' to work and don't let it
                    ;; `icomplete-exhibit' anything.
                    (enable-recursive-minibuffers t)
                    (mycomplete-mode nil))
                (funcall action))
          (completion--cache-all-sorted-completions
           (mycomplete--field-beg)
           (mycomplete--field-end)
           (cdr all)))
        (message nil)))))

;; Adapted from choose-completion
(defun mycomplete-completions-fido-ret ()
  (interactive)
  (if (and (active-minibuffer-window)
           (with-selected-window (active-minibuffer-window)
             (eq (mycomplete--category) 'file)))
      (let* ((buffer completion-reference-buffer)
             (current
              (let (beg)
                (cond
                 ((and (not (eobp))
                       (get-text-property (point) 'completion--string))
                  (setq beg (1+ (point))))
                 ((and (not (bobp))
                       (get-text-property (1- (point)) 'completion--string))
                  (setq beg (point)))
                 (t (error "No completion here")))
                (setq beg (or (previous-single-property-change
                               beg 'completion--string)
                              beg))
                (substring-no-properties
                 (get-text-property beg 'completion--string)))))
        (quit-window)
        (with-current-buffer buffer
          (let* ((dir (file-name-directory
                       (let ((str (mycomplete--field-string)))
                         (if (and rfn-eshadow-overlay (overlay-buffer rfn-eshadow-overlay))
                             (seq-subseq str (- (overlay-end rfn-eshadow-overlay)
                                                (overlay-start rfn-eshadow-overlay)))
                           str))))
                 (probe (and dir current
                             (expand-file-name (directory-file-name current)
                                               (substitute-env-vars dir)))))
            (goto-char (mycomplete--field-beg))
            (delete-region (mycomplete--field-beg)
                           (mycomplete--field-end))
            (insert (concat probe "/"))
            (unless (and probe (file-directory-p probe) (not (string= current "./")))
              (exit-minibuffer)))))
    (choose-completion)
    (when (active-minibuffer-window)
      (minibuffer-force-complete-and-exit))))

(defun mycomplete-ret ()
  "Exit minibuffer for icomplete."
  (interactive)
  (if (and mycomplete-show-matches-on-no-input
           (car completion-all-sorted-completions)
           (equal (mycomplete--field-string) mycomplete--initial-input))
      (mycomplete-force-complete-and-exit)
    (minibuffer-complete-and-exit)))

(defun mycomplete-copy-or-kill (beg end)
  (interactive (list (region-beginning) (region-end)))
  (if (use-region-p)
      (kill-region beg end)
    (kill-new (car (completion-all-sorted-completions)))))

(provide 'mycomplete)
